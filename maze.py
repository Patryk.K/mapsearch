import numpy as np
from numpy.random import randint as rand


class Maze:
    def __init__(self, width, height, complexity, density):
        self.width = width
        self.height = height
        self.shape = ((height // 2) * 2 + 1, (width // 2) * 2 + 1)
        self.complexity = int(complexity * (5 * (self.shape[0] + self.shape[1])))
        self.density = int(density * ((self.shape[0] // 2) * (self.shape[1] // 2)))
        self.map = np.zeros(self.shape, dtype=bool)
        self.grid = np.zeros(((self.width//2)*2 + 1, (self.height//2)*2 + 1))
        self.goal = [None, None]
        self.start = [None, None]

        # description of possible values
        self.emptyValue = 0
        self.boarderValue = 1
        self.startValue = 2
        self.visitedValue = 3
        self.goalValue = 4

    def generateMap(self):
        self.map[0, :] = self.map[-1, :] = self.boarderValue
        self.map[:, 0] = self.map[:, -1] = self.boarderValue
        for _ in range(self.density):
            x, y = rand(0, self.shape[1] // 2) * 2, rand(0, self.shape[0] // 2) * 2  # Pick a random position
            self.map[y, x] = self.boarderValue
            for j in range(self.complexity):
                neighbours = []
                if x > 1:
                    neighbours.append((y, x - 2))
                if x < self.shape[1] - 2:
                    neighbours.append((y, x + 2))
                if y > 1:
                    neighbours.append((y - 2, x))
                if y < self.shape[0] - 2:
                    neighbours.append((y + 2, x))
                if len(neighbours):
                    y_, x_ = neighbours[rand(0, len(neighbours) - 1)]
                    if self.map[y_, x_] == self.emptyValue:
                        self.map[y_, x_] = self.boarderValue
                        self.map[y_ + (y - y_) // 2, x_ + (x - x_) // 2] = self.boarderValue
                        x, y = x_, y_

    def setupGrid(self):
        for x in range((self.width // 2) * 2 + 1):
            for y in range((self.height // 2) * 2 + 1):
                if self.map[x][y] == self.boarderValue:
                    self.grid[x][y] = self.boarderValue
        x, y = self.setValueAtRandomGridPosition(4)
        self.goal = [x, y]
        x, y = self.setValueAtRandomGridPosition(2)
        self.start = [x, y]

    def search(self, x=None, y=None):
        if x is None and y is None:
            x = self.start[0]
            y = self.start[1]

        if self.grid[x][y] == self.goalValue:
            return True

        elif self.grid[x][y] == self.boarderValue:
            return False

        elif self.grid[x][y] == self.visitedValue:
            return False
        if self.grid[x][y] != self.startValue:
            self.grid[x][y] = self.visitedValue

        if ((x < len(self.grid) - 1 and self.search(x + 1, y))
                or (y > 0 and self.search(x, y - 1))
                or (x > 0 and self.search(x - 1, y))
                or (y < len(self.grid) - 1 and self.search(x, y + 1))):
            return True

        return False

    def setValueAtRandomGridPosition(self, value):
        while True:
            x, y = rand(1, self.width) - 1, rand(1, self.height) - 1
            if self.grid[x][y] != 1 and self.grid[x][y] != 2 and self.grid[x][y] != 4:
                self.grid[x][y] = value
                return x, y

    def setStartPosition(self, setX, setY):
        if self.grid[setX][setY] != 1 and self.grid[setX][setY] != 2 and self.grid[setX][setY] != 4:
            self.grid[self.start[0]][self.start[1]] = self.emptyValue
            self.grid[setX][setY] = self.startValue
        else:
            print("Cant change start point")

    def setGoalPosition(self, setX, setY):
        if self.grid[setX][setY] != 1 and self.grid[setX][setY] != 2 and self.grid[setX][setY] != 4:
            self.grid[self.goal[0]][self.goal[1]] = self.emptyValue
            self.grid[setX][setY] = self.goalValue
        else:
            print("Cant change goal point")

    def getGrid(self):
        return self.grid

    def getWidth(self):
        return self.width

    def getHeight(self):
        return self.height

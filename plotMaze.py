import matplotlib.pyplot as plt
from shapely.geometry.polygon import Polygon


class PlotMaze:
    def __init__(self, grid, width, height):
        self.grid = grid
        self.width = width
        self.height = height

        self.fig = plt.figure(1, figsize=(6, 6), dpi=90)
        self.ax = self.fig.add_subplot(111)

    def drawOctagonNotFilled(self,x, y):
        poly = Polygon(
            [(x + 0, y + 0), (x + 0, y + 0.33), (x + 0.33, y + 0.66), (x + 0.66, y + 0.66), (x + 0.99, y + 0.33),
             (x + 0.99, y + 0), (x + 0.66, y + -0.33), (x + 0.33, y + -0.33), (x + 0, y + 0)])
        x, y = poly.exterior.xy
        self.ax.plot(x, y, color='#6699cc', alpha=0.7, linewidth=1, solid_capstyle='round', zorder=2)

    def drawOctagonFilled(self, x, y, color):
        poly = Polygon(
            [(x + 0, y + 0), (x + 0, y + 0.33), (x + 0.33, y + 0.66), (x + 0.66, y + 0.66), (x + 0.99, y + 0.33),
             (x + 0.99, y + 0), (x + 0.66, y + -0.33), (x + 0.33, y + -0.33), (x + 0, y + 0)])
        x, y = poly.exterior.xy
        self.ax.plot(x, y, color='#6699cc', alpha=0.7, linewidth=1, solid_capstyle='round', zorder=2)
        plt.fill(x, y, color)

    def fillGrid(self):
        for x in range((self.width // 2) * 2 + 1):
            for y in range((self.height // 2) * 2 + 1):
                if self.grid[x][y] == 0:
                    self.drawOctagonNotFilled(x, y)
                elif self.grid[x][y] == 1:
                    self.drawOctagonFilled(x, y, color='b')
                elif self.grid[x][y] == 2:
                    self.drawOctagonFilled(x, y, color='g')
                elif self.grid[x][y] == 3:
                    self.drawOctagonFilled(x, y, color='r')
                elif self.grid[x][y] == 4:
                    self.drawOctagonFilled(x, y, color='y')

    @staticmethod
    def showPlot():
        plt.show()
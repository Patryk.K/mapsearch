from maze import Maze
from plotMaze import PlotMaze

if __name__ == '__main__':
    maze = Maze(25, 25, 75, .75)
    maze.generateMap()
    maze.setupGrid()
    # maze.setStartPosition(5, 6)
    # maze.setGoalPosition(11, 16)
    maze.search()

    plotMaze = PlotMaze(maze.getGrid(), maze.getWidth(), maze.getHeight())
    plotMaze.fillGrid()

    plotMaze.showPlot()
